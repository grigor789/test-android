package com.example.basicproject2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;

import android.database.Cursor;
import android.os.Bundle;

import com.example.basicproject2.constants.Constants;
import com.example.basicproject2.fragments.fragments.inside.HomeFragment;
import com.example.basicproject2.fragments.fragments.outside.LoginFragment;
import com.example.basicproject2.model.ListContentsModel;
import com.example.basicproject2.viewModel.AppViewModel;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements HomeFragment.OnBackPressed {
    private DrawerLayout drawerLayout;
    AppViewModel appViewModel;
    ArrayList<ListContentsModel> arrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
      appViewModel = new ViewModelProvider(this).get(AppViewModel.class);
      arrayList=new ArrayList<>();

        thread.start();
        LoginFragment loginFragment = new LoginFragment();
getSupportFragmentManager().beginTransaction().replace(R.id.container1, loginFragment).commit();


    }

    @Override
    public void onBackPressed() {
        if(drawerLayout!=null&&drawerLayout.isDrawerOpen(GravityCompat.START)){drawerLayout.closeDrawers();}
        else {getSupportFragmentManager().popBackStack();}

    }

    @Override
    public void onBackPressed(DrawerLayout drawerLayout) {
        this.drawerLayout=drawerLayout;
    }
    Thread thread=new Thread(new Runnable() {
        @Override
        public void run() {Cursor cursor= appViewModel.readContent();
        while (cursor.moveToNext()){
        int id=cursor.getInt(cursor.getColumnIndex(Constants.Constant.ID));
        int favorite=cursor.getInt(cursor.getColumnIndex(Constants.Constant.Favorite));
        String category1=cursor.getString(cursor.getColumnIndex(Constants.Constant.Category1));
        String category2 =cursor.getString(cursor.getColumnIndex(Constants.Constant.Category2));
        String text=cursor.getString(cursor.getColumnIndex(Constants.Constant.PicName));
        int image=cursor.getInt(cursor.getColumnIndex(Constants.Constant.Picture));
        arrayList.add(new ListContentsModel(id,favorite,category1,category2,text,image));}

           if(arrayList.size()==0){ addContent();}


    }});
    private void addContent() {
        //bmw
        appViewModel.addContent(0, "BMW 2", R.drawable.bmw_2, "car", "bmw");
        appViewModel.addContent(0, "BMW 3", R.drawable.bmw_3, "car", "bmw");
        appViewModel.addContent(0, "BMW 4", R.drawable.bmw_4, "car", "bmw");
        appViewModel.addContent(0, "BMW 5", R.drawable.bmw_5, "car", "bmw");
        appViewModel.addContent(0, "BMW 6", R.drawable.bmw_6, "car", "bmw");
        appViewModel.addContent(0, "BMW 7", R.drawable.bmw_7, "car", "bmw");

        //mercedes
        appViewModel.addContent(0, "Mercedes a", R.drawable.mercedes_a, "car", "mercedes");
        appViewModel.addContent(0, "Mercedes b", R.drawable.mercedes_b, "car", "mercedes");
        appViewModel.addContent(0, "Mercedes c", R.drawable.mercedes_c, "car", "mercedes");
        appViewModel.addContent(0, "Mercedes cla", R.drawable.mercedes_cla, "car", "mercedes");
        appViewModel.addContent(0, "Mercedes cls", R.drawable.mercedes_cls, "car", "mercedes");
        appViewModel.addContent(0, "Mercedes e", R.drawable.mercedes_e, "car", "mercedes");

        //toyota

        appViewModel.addContent(0, "Toyota avalon", R.drawable.toyota_avalon, "car", "toyota");
        appViewModel.addContent(0, "Toyota camry", R.drawable.toyota_camry, "car", "toyota");
        appViewModel.addContent(0, "Toyota chr", R.drawable.toyota_chr, "car", "toyota");
        appViewModel.addContent(0, "Toyota corolla", R.drawable.toyota_corolla, "car", "toyota");
        appViewModel.addContent(0, "Toyota prado", R.drawable.toyota_prado, "car", "toyota");
        appViewModel.addContent(0, "Toyota rav4", R.drawable.toyota_rav4, "car", "toyota");

        //phone
        //iphone
        appViewModel.addContent(0,"Iphone 3g",R.drawable.iphone3g,"phone","iphone");
        appViewModel.addContent(0,"Iphone 4",R.drawable.iphone4,"phone","iphone");
        appViewModel.addContent(0,"Iphone 5",R.drawable.iphone5,"phone","iphone");
        appViewModel.addContent(0,"Iphone 6",R.drawable.iphone6,"phone","iphone");
        appViewModel.addContent(0,"Iphone 7",R.drawable.iphone7,"phone","iphone");
        appViewModel.addContent(0,"Iphone 11",R.drawable.iphone11,"phone","iphone");

        //samsung

        appViewModel.addContent(0,"Galaxy s5",R.drawable.galaxys5,"phone","samsung");
        appViewModel.addContent(0,"Galaxy s6",R.drawable.galaxys6,"phone","samsung");
        appViewModel.addContent(0,"Galaxy s7",R.drawable.galaxys7,"phone","samsung");
        appViewModel.addContent(0,"Galaxy s8",R.drawable.galaxys8,"phone","samsung");
        appViewModel.addContent(0,"Galaxy s9",R.drawable.galaxys9,"phone","samsung");
        appViewModel.addContent(0,"Galaxy s10",R.drawable.galaxys10,"phone","samsung");

        //nokia
        appViewModel.addContent(0,"Nokia 1011",R.drawable.nokia1011,"phone","nokia");
        appViewModel.addContent(0,"Nokia 3310",R.drawable.nokia3310,"phone","nokia");
        appViewModel.addContent(0,"Nokia 6800",R.drawable.nokia6800,"phone","nokia");
        appViewModel.addContent(0,"Nokia 7600",R.drawable.nokia7600,"phone","nokia");
        appViewModel.addContent(0,"Nokia 7650",R.drawable.nokia7650,"phone","nokia");
        appViewModel.addContent(0,"Nokia 8110",R.drawable.nokia8110,"phone","nokia");

        //england
        appViewModel.addContent(0,"Arsenal",R.drawable.arsenal,"football","england");
        appViewModel.addContent(0,"Chelsea",R.drawable.chelsea,"football","england");
        appViewModel.addContent(0,"Manchester City",R.drawable.manchestercity,"football","england");
        appViewModel.addContent(0,"Manchester United",R.drawable.manchester,"football","england");
        appViewModel.addContent(0,"Tottenham",R.drawable.tottenham,"football","england");

        //germany
        appViewModel.addContent(0,"Bayern",R.drawable.bayern,"football","germany");
        appViewModel.addContent(0,"Borussia",R.drawable.borusia,"football","germany");
        appViewModel.addContent(0,"Hoffenheim",R.drawable.hoffenheim,"football","germany");
        appViewModel.addContent(0,"Leipzig",R.drawable.leipzig,"football","germany");
        appViewModel.addContent(0,"Volksburg",R.drawable.volksburg,"football","germany");

        //spain
        appViewModel.addContent(0,"Atletico",R.drawable.atletico,"football","spain");
        appViewModel.addContent(0,"Barcellona",R.drawable.barcellona,"football","spain");
        appViewModel.addContent(0,"Espanyol",R.drawable.espanyol,"football","spain");
        appViewModel.addContent(0,"RealMadrid",R.drawable.realmadrid,"football","spain");
        appViewModel.addContent(0,"valencia",R.drawable.valencia,"football","spain");

    }

}