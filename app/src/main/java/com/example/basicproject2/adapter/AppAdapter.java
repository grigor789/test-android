package com.example.basicproject2.adapter;


import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.example.basicproject2.R;
import com.example.basicproject2.constants.Constants;
import com.example.basicproject2.model.ListContentsModel;
import com.example.basicproject2.viewModel.AppViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AppAdapter extends RecyclerView.Adapter<AppAdapter.AppHolder> {
    private List<ListContentsModel> arrayList;
    private AppViewModel appViewModel;
    private ArrayList<ListContentsModel> favoriteList = new ArrayList<>();

    public AppAdapter(AppViewModel appViewModel) {
        this.appViewModel = appViewModel;
    }

    public static class AppHolder extends RecyclerView.ViewHolder {
        private AppCompatImageView bigImage;
        private AppCompatImageView favorite;
        private AppCompatTextView textNameView;

        public AppHolder(@NonNull View itemView) {
            super(itemView);
            bigImage = itemView.findViewById(R.id.imageViewBig);
            favorite = itemView.findViewById(R.id.favoriteHeart);
            textNameView = itemView.findViewById(R.id.textViewPictureName);

        }
    }


    @NotNull
    @Override
    public AppHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview1, parent, false);
        return new AppHolder(view);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public void setData(ArrayList<ListContentsModel> arrayList) {
        this.arrayList = arrayList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(AppHolder holder, int position) {

        ListContentsModel listContentsModel = arrayList.get(position);
        holder.bigImage.setImageResource(listContentsModel.getImage());
        holder.textNameView.setText(listContentsModel.getText());

        if (listContentsModel.getFavorite() == 0) {
            holder.favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
        } else {
            holder.favorite.setImageResource(R.drawable.ic_baseline_favorite_24);
        }
        holder.favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listContentsModel.getFavorite() == 0) {
                    holder.favorite.setImageResource(R.drawable.ic_baseline_favorite_24);
                    listContentsModel.setFavorite(1);
                    update(listContentsModel, true);


                } else {
                    holder.favorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                    listContentsModel.setFavorite(0);
                    update(listContentsModel, false);
                }
            }

            private void update(ListContentsModel listContentsModel, Boolean t) {
                String id = String.valueOf(listContentsModel.getId());
                int favorite = listContentsModel.getFavorite();
                String category1 = listContentsModel.getCategory1();
                String category2 = listContentsModel.getCategory2();
                String text = listContentsModel.getText();
                int image = listContentsModel.getImage();
                appViewModel.updateContent(favorite, text, image, category1, category2, id);
                if (t) {
                    arrayList.get(position).setFavorite(1);

                } else if (!t) {
                    arrayList.get(position).setFavorite(0);

                }
            }


        });
    }

    public void clicked(Boolean clicked, String key) {
        if (clicked) {
            if (!arrayList.isEmpty()) {if(!favoriteList.isEmpty()){favoriteList.clear();}
                for (int i = 0; i < arrayList.size(); i++) {
                    if (arrayList.get(i).getFavorite() == 1 && arrayList.get(i).getCategory2().equals(key) && !key.equals(Constants.Constant.All)) {
                        favoriteList.add(arrayList.get(i));
                    }else  if (arrayList.get(i).getFavorite() == 1 && key.equals(Constants.Constant.All) ){favoriteList.add(arrayList.get(i));}
                }
            }

            arrayList = favoriteList;
            notifyDataSetChanged();

        }


    }
}

