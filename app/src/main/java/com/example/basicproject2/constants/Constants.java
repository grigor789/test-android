package com.example.basicproject2.constants;

public class Constants {
    public static class Constant{
        public static final String TABLE_NAME="UserInfo";
        public static final String UserName="UserName";
        public static final String Password="Password";
        public static final String Name="Name";
        public static final String LastName="LastName";
        public static final String Age="Age";
        public static final String Boolean="boolean";
        public static final String ID="ID";
        public static final String Favorite="Favorite";
        public static final String Category1="Category1";
        public static final String Category2="Category2";
        public static final String TABLE_NAME2="ContentInfo";
        public static final String PicName="PictureName";
        public static final String Picture="Picture";
        public static final String CarFragment="CarFragment";
        public static final String PhoneFragment="PhoneFragment";
        public static final String FootballFragment="FootBallFragment";
        public static final String Bmw="bmw";
        public static final String Mercedes="mercedes";
        public static final String Toyota="toyota";
        public static final String Key="key";
        public static final String Iphone="iphone";
        public static final String Samsung="samsung";
        public static final String Nokia="nokia";
        public static final String Germany="germany";
        public static final String Spain="spain";
        public static final String England="england";
        public static final String All="all";
        public static final String LoggedOut="loggedOut";


    }
}
