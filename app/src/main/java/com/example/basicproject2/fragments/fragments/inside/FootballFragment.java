package com.example.basicproject2.fragments.fragments.inside;

import android.database.Cursor;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.basicproject2.R;
import com.example.basicproject2.adapter.AppAdapter;
import com.example.basicproject2.constants.Constants;
import com.example.basicproject2.model.ListContentsModel;
import com.example.basicproject2.viewModel.AppViewModel;

import java.util.ArrayList;


public class FootballFragment extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private AppAdapter adapter;
    private AppViewModel viewModel;
    private ArrayList<ListContentsModel> arrayList;
    private ArrayList<ListContentsModel> arrayList3;
    private ArrayList<ListContentsModel> arrayList2;
    private String key;
    private Boolean clicked;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_football, container, false);
        init(view);


        return view;
    }


    private void init(View view) {

        arrayList = new ArrayList<>();
        arrayList2 = new ArrayList<>();
        arrayList3 =new ArrayList<>();
        viewModel = new ViewModelProvider(this).get(AppViewModel.class);

        Cursor cursor = viewModel.footballContent();


        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(Constants.Constant.ID));
            int favorite = cursor.getInt(cursor.getColumnIndex(Constants.Constant.Favorite));
            String category1 = cursor.getString(cursor.getColumnIndex(Constants.Constant.Category1));
            String category2 = cursor.getString(cursor.getColumnIndex(Constants.Constant.Category2));
            String text = cursor.getString(cursor.getColumnIndex(Constants.Constant.PicName));
            int image = cursor.getInt(cursor.getColumnIndex(Constants.Constant.Picture));
            arrayList.add(new ListContentsModel(id, favorite, category1, category2, text, image));

        }
        AppViewModel appViewModel = new ViewModelProvider(this).get(AppViewModel.class);
        recyclerView = view.findViewById(R.id.recyclerViewFootball);
        adapter = new AppAdapter(appViewModel);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setData(arrayList);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            key = getArguments().getString(Constants.Constant.Key);
            clicked = getArguments().getBoolean(Constants.Constant.Boolean);
        }


        if (key != null && key.equals(Constants.Constant.England)) {
            if (!arrayList2.isEmpty()) {
                arrayList2.clear();
            }
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).getCategory2().equals(Constants.Constant.England)) {
                    arrayList2.add(arrayList.get(i));
                }

            }
            adapter.setData(arrayList2);
        }
        if (key != null && key.equals(Constants.Constant.Germany)) {
            if (!arrayList2.isEmpty()) {
                arrayList2.clear();
            }
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).getCategory2().equals(Constants.Constant.Germany)) {
                    arrayList2.add(arrayList.get(i));
                }

            }
            adapter.setData(arrayList2);
        }
        if (key != null && key.equals(Constants.Constant.Spain)) {
            if (!arrayList2.isEmpty()) {
                arrayList2.clear();
            }
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).getCategory2().equals(Constants.Constant.Spain)) {
                    arrayList2.add(arrayList.get(i));
                }

            }
            adapter.setData(arrayList2);
        }
        if (getArguments() != null) {
            key = getArguments().getString(Constants.Constant.Key);
        }
        if (key != null && key.equals(Constants.Constant.All)) {
            adapter.setData(arrayList);
        }
        if (clicked != null) {

            adapter.clicked(clicked, key);
        }



    }}








