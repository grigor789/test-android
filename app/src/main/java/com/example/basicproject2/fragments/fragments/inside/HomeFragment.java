package com.example.basicproject2.fragments.fragments.inside;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.basicproject2.R;
import com.example.basicproject2.constants.Constants;

import com.example.basicproject2.fragments.fragments.outside.LoginFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import java.util.Objects;


public class HomeFragment extends Fragment implements NavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener {

    private CarFragment carFragment;
    private PhoneFragment phoneFragment;
    private FootballFragment footballFragment;
    private DrawerLayout drawerLayout;
    private OnBackPressed onBackPressedListener;
    private Bundle bundle;
    private String fragment = Constants.Constant.CarFragment;
    private boolean clicked = false;
    String key = Constants.Constant.All;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        init(view);

        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.container3, carFragment).commit();

        return view;
    }

    private void init(View view) {


        drawerLayout = view.findViewById(R.id.drawer_layout);
        NavigationView navigationView = view.findViewById(R.id.navigationView);
        Toolbar toolbar = view.findViewById(R.id.tool_bar);
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        carFragment = new CarFragment();
        footballFragment = new FootballFragment();
        phoneFragment = new PhoneFragment();
        bundle = new Bundle();
        BottomNavigationView bottomNavigationView = view.findViewById(R.id.bottom_Nav);
        navigationView.setNavigationItemSelectedListener(this);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        setHasOptionsMenu(true);
        onBackPressedListener.onBackPressed(drawerLayout);


    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.car:{
                fragment = Constants.Constant.CarFragment;
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction().replace(R.id.container3, carFragment).addToBackStack(null).commit();
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                }}
                break;
            case R.id.phone:{
                fragment = Constants.Constant.PhoneFragment;
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction().replace(R.id.container3, phoneFragment).addToBackStack(null).commit();
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                }
               } break;
            case R.id.football:{
                fragment = Constants.Constant.FootballFragment;
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction().replace(R.id.container3, footballFragment).addToBackStack(null).commit();
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                }
                }break;

            case R.id.fragment1: {

                if (fragment.equals(Constants.Constant.CarFragment)) {
                    bundle.putString(Constants.Constant.Key, Constants.Constant.All);
                    key = Constants.Constant.All;
                    clicked = false;
                    bundle.putBoolean(Constants.Constant.Boolean, clicked);
                    carFragment.setArguments(bundle);
                    carFragment.onCreate(null);

                }
                if (fragment.equals(Constants.Constant.PhoneFragment)) {
                    bundle.putString(Constants.Constant.Key, Constants.Constant.All);
                    clicked = false;
                    key = Constants.Constant.All;
                    bundle.putBoolean(Constants.Constant.Boolean, clicked);
                    phoneFragment.setArguments(bundle);
                    phoneFragment.onCreate(null);
                }
                if (fragment.equals(Constants.Constant.FootballFragment)) {
                    bundle.putString(Constants.Constant.Key, Constants.Constant.All);
                    clicked = false;
                    key = Constants.Constant.All;
                    bundle.putBoolean(Constants.Constant.Boolean, clicked);
                    footballFragment.setArguments(bundle);
                    footballFragment.onCreate(null);
                   }

                break;

            }


            case R.id.fragment2: {

                if (fragment.equals(Constants.Constant.CarFragment)) {
                    bundle.putString(Constants.Constant.Key, Constants.Constant.Bmw);
                    clicked = false;
                    key = Constants.Constant.Bmw;
                    bundle.putBoolean(Constants.Constant.Boolean, clicked);
                    carFragment.setArguments(bundle);
                    carFragment.onCreate(null);

                }
                if (fragment.equals(Constants.Constant.PhoneFragment)) {
                    bundle.putString(Constants.Constant.Key, Constants.Constant.Iphone);
                    clicked = false;
                    bundle.putBoolean(Constants.Constant.Boolean, clicked);
                    key = Constants.Constant.Iphone;
                    phoneFragment.setArguments(bundle);
                    phoneFragment.onCreate(null);
                }
                if (fragment.equals(Constants.Constant.FootballFragment)) {
                    bundle.putString(Constants.Constant.Key, Constants.Constant.Germany);
                    clicked = false;
                    key = Constants.Constant.Germany;
                    bundle.putBoolean(Constants.Constant.Boolean, clicked);
                    footballFragment.setArguments(bundle);
                    footballFragment.onCreate(null);
                }

                break; }
            case R.id.fragment3: {

                if (fragment.equals(Constants.Constant.CarFragment)) {
                    bundle.putString(Constants.Constant.Key, Constants.Constant.Mercedes);
                    clicked = false;
                    key = Constants.Constant.Mercedes;
                    bundle.putBoolean(Constants.Constant.Boolean, clicked);
                    carFragment.setArguments(bundle);
                    carFragment.onCreate(null);

                }
                if (fragment.equals(Constants.Constant.PhoneFragment)) {
                    bundle.putString(Constants.Constant.Key, Constants.Constant.Samsung);
                    clicked = false;
                    bundle.putBoolean(Constants.Constant.Boolean, clicked);
                    key = Constants.Constant.Samsung;
                    phoneFragment.setArguments(bundle);
                    phoneFragment.onCreate(null);
                }
                if (fragment.equals(Constants.Constant.FootballFragment)) {
                    bundle.putString(Constants.Constant.Key, Constants.Constant.England);
                    clicked = false;
                    key = Constants.Constant.England;
                    bundle.putBoolean(Constants.Constant.Boolean, clicked);
                    footballFragment.setArguments(bundle);
                    footballFragment.onCreate(null);
                }
                break;
            }

            case R.id.fragment4: {

                if (fragment.equals(Constants.Constant.CarFragment)) {
                    bundle.putString(Constants.Constant.Key, Constants.Constant.Toyota);
                    clicked = false;
                    bundle.putBoolean(Constants.Constant.Boolean, clicked);
                    key = Constants.Constant.Toyota;
                    carFragment.setArguments(bundle);
                    carFragment.onCreate(null);

                }
                if (fragment.equals(Constants.Constant.PhoneFragment)) {
                    bundle.putString(Constants.Constant.Key, Constants.Constant.Nokia);
                    clicked = false;
                    bundle.putBoolean(Constants.Constant.Boolean, clicked);
                    key = Constants.Constant.Nokia;
                    phoneFragment.setArguments(bundle);
                    phoneFragment.onCreate(null);
                }
                if (fragment.equals(Constants.Constant.FootballFragment)) {
                    bundle.putString(Constants.Constant.Key, Constants.Constant.Spain);
                    clicked = false;
                    bundle.putBoolean(Constants.Constant.Boolean, clicked);
                    key = Constants.Constant.Spain;
                    footballFragment.setArguments(bundle);
                    footballFragment.onCreate(null);
                }
                break;
            }

        }
        return true;
    }


    public interface OnBackPressed {
        void onBackPressed(DrawerLayout drawerLayout);

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            onBackPressedListener = (OnBackPressed) context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.general, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.favoriteList: {
                Bundle bundle1 = new Bundle();
                clicked = true;
                if (fragment.equals(Constants.Constant.CarFragment)) {

                    bundle1.putBoolean(Constants.Constant.Boolean, clicked);
                    bundle1.putString(Constants.Constant.Key, key);
                    carFragment.setArguments(bundle1);
                    carFragment.onCreate(null);
                }
                else if (fragment.equals(Constants.Constant.FootballFragment)) {
                    bundle1.putBoolean(Constants.Constant.Boolean, clicked);
                    bundle1.putString(Constants.Constant.Key, key);
                    footballFragment.setArguments(bundle1);
                    footballFragment.onCreate(null);
                }
                else if(fragment.equals(Constants.Constant.PhoneFragment)) {
                    bundle1.putBoolean(Constants.Constant.Boolean, clicked);
                    bundle1.putString(Constants.Constant.Key, key);
                    phoneFragment.setArguments(bundle1);
                    phoneFragment.onCreate(null);
                }

            }return  true;

            case R.id.logOut: {
                LoginFragment loginFragment=new LoginFragment();
                Bundle bundle2=new Bundle();
                bundle2.putString(Constants.Constant.Key, Constants.Constant.LoggedOut);
                loginFragment.setArguments(bundle2);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container1,loginFragment).commit();
            }return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
