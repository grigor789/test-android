package com.example.basicproject2.fragments.fragments.outside;

import android.database.Cursor;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.basicproject2.R;
import com.example.basicproject2.constants.Constants;
import com.example.basicproject2.viewModel.AppViewModel;
import com.google.android.material.textfield.TextInputLayout;


public class ForgotFragment extends Fragment {
private TextInputLayout username;
private TextInputLayout newPassword;
private TextInputLayout confirmPassword;
private AppCompatButton confirmButton;

private AppViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_forgot_password, container, false);
        init(view);
    return view;
    }

    private void init(View view) {
        viewModel=new ViewModelProvider(this).get(AppViewModel.class);
        username=view.findViewById(R.id.forgot_username);
        newPassword=view.findViewById(R.id.forgot_password);
        confirmPassword=view.findViewById(R.id.new_password);
        confirmButton=view.findViewById(R.id.btn_confirm);
        confirmButton.setOnClickListener(onClickListener);

    }

    private View.OnClickListener onClickListener= view -> {
        Cursor cursor=viewModel.readUser();

        while (cursor.moveToNext()){

            String username1=cursor.getString(cursor.getColumnIndex(Constants.Constant.UserName));
            String password=newPassword.getEditText().getText().toString();
            if(username.getEditText().getText().toString().equals(username1)){
                String id=String.valueOf(cursor.getInt(cursor.getColumnIndex(Constants.Constant.ID)));

                String name=cursor.getString(cursor.getColumnIndex(Constants.Constant.Name));
                String lastName=cursor.getString(cursor.getColumnIndex(Constants.Constant.LastName));
                String age=cursor.getString(cursor.getColumnIndex(Constants.Constant.Age));
                if(password.equals(confirmPassword.getEditText().getText().toString())){
                viewModel.updateUser(username1,password,name,lastName,age,id);
                getActivity().getSupportFragmentManager().popBackStack();
                }else
                {confirmPassword.setError("passwords don't match ");}
            }
        }




    };}
