package com.example.basicproject2.fragments.fragments.outside;


import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;


import com.example.basicproject2.R;
import com.example.basicproject2.constants.Constants;

import com.example.basicproject2.fragments.fragments.inside.HomeFragment;
import com.example.basicproject2.viewModel.AppViewModel;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Map;


public class LoginFragment extends Fragment implements View.OnClickListener, AppCompatCheckBox.OnCheckedChangeListener {
    private TextInputLayout username;
    private TextInputLayout password;
    private AppViewModel appViewModel;
    private ArrayList<String> userList;
    private ArrayList<String> passwordList;
    private AppCompatButton login;
    private AppCompatButton signIn;
    private AppCompatButton forgot;
    private AppCompatCheckBox rememberCheckBox;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    String getKey;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_login, container, false);
        init(view);


        if (getArguments() != null) {
            getKey = getArguments().getString(Constants.Constant.Key);
        }
        if (getKey!=null&&getKey.equals(Constants.Constant.LoggedOut)) {
            mEditor.clear();
            mEditor.commit();
        }
        if (!mPreferences.getAll().isEmpty()) {
            transferHome();
        }




        return view;
    }


    private void init(View view) {
        username = view.findViewById(R.id.editText_logIn);
        password = view.findViewById(R.id.editText_password);
        userList = new ArrayList<>();
        passwordList = new ArrayList<>();
        login = view.findViewById(R.id.btn_logIn);
        signIn = view.findViewById(R.id.btn_signIN);
        forgot = view.findViewById(R.id.btn_forgotPassword);
        rememberCheckBox = view.findViewById(R.id.rememberCheckBox);
        login.setOnClickListener(this);
        signIn.setOnClickListener(this);
        forgot.setOnClickListener(this);
        rememberCheckBox.setOnCheckedChangeListener(this);
        mPreferences = getActivity().getSharedPreferences("gui", Context.MODE_PRIVATE);
        mEditor = mPreferences.edit();
    }

    private void readUser() {
        appViewModel = new ViewModelProvider(this).get(AppViewModel.class);
        Cursor cursor = appViewModel.readUser();
        while (cursor.moveToNext()) {
            userList.add(cursor.getString(cursor.getColumnIndex(Constants.Constant.UserName)));
            passwordList.add(cursor.getString(cursor.getColumnIndex(Constants.Constant.Password)));
        }


    }

    private Boolean checkUser() {
        String getUsername = username.getEditText().getText().toString();
        String getPassword = password.getEditText().getText().toString();
        if (userList.contains(getUsername) && !passwordList.contains(getPassword)) {
            password.setError("password is incorrect");
            return false;
        } else if (!userList.contains(getUsername) && passwordList.contains(getPassword)) {
            username.setError("username is incorrect");
            return false;
        } else if (!userList.contains(getUsername) && !passwordList.contains(getPassword)) {
            username.setError("username is incorrect");
            password.setError("password is incorrect");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_logIn:
                readUser();

                if (checkUser()) {
                    transferHome();
                }
                ;
                break;
            case R.id.btn_signIN:
                transferSignIn();
                break;
            case R.id.btn_forgotPassword:
                transferForgot();
                break;

        }
    }

    private void transferHome() {
        HomeFragment homeFragment = new HomeFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.container1, homeFragment).commit();
    }

    private void transferSignIn() {
        SignInFragment signInFragment = new SignInFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container1, signInFragment).addToBackStack(null).commit();
    }

    private void transferForgot() {
        ForgotFragment forgotFragment = new ForgotFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container1, forgotFragment).addToBackStack(null).commit();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (compoundButton.isChecked()) {
            mEditor.putString("username", "1");
            mEditor.putString("password", "2");
            mEditor.commit();

        } else if (!compoundButton.isChecked()) {
            mEditor.clear();

            mEditor.commit();


        }

    }

}