package com.example.basicproject2.fragments.fragments.outside;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import com.example.basicproject2.R;
import com.example.basicproject2.constants.Constants;
import com.example.basicproject2.viewModel.AppViewModel;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;


public class SignInFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
private TextInputLayout nameText;
private TextInputLayout surnameText;
private TextInputLayout usernameText;
private TextInputLayout passwordText;
private TextInputLayout confirmPasswordText;
private MaterialTextView birthTextView;
private AppViewModel appViewModel;
private ArrayList<String> arrayList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       View view= inflater.inflate(R.layout.fragment_sign_in, container, false);
       init(view);

       return view;

    }

    private void init(View view) {
        nameText=view.findViewById(R.id.setUserName);
        surnameText=view.findViewById(R.id.setUserSureName);
        usernameText=view.findViewById(R.id.setUserNameLogin);
        passwordText=view.findViewById(R.id.setPassword1);
        confirmPasswordText=view.findViewById(R.id.confirmPassword1);
        birthTextView=view.findViewById(R.id.birthDate);
        arrayList=new ArrayList<>();
        AppCompatButton register = view.findViewById(R.id.btn_register);
        register.setOnClickListener(this);
         appViewModel = new ViewModelProvider(this).get(AppViewModel.class);
         birthTextView.setOnClickListener(this);

    }
    private void addUser() {
        String name= Objects.requireNonNull(nameText.getEditText()).getText().toString().trim();
        String surname= Objects.requireNonNull(surnameText.getEditText()).getText().toString().trim();
        String username= Objects.requireNonNull(usernameText.getEditText()).getText().toString().trim();
        String password= Objects.requireNonNull(passwordText.getEditText()).getText().toString().trim();
        String confirmPassword= Objects.requireNonNull(confirmPasswordText.getEditText()).getText().toString().trim();
        String birthDate=birthTextView.getText().toString().trim();
        if(!password.equals(confirmPassword)){confirmPasswordText.setError("passwords don't match");}
        if(name.isEmpty()){nameText.setError("name is empty");}
        if(surname.isEmpty()){surnameText.setError("surname is empty");}
        if(username.isEmpty()){usernameText.setError("username is empty");}
        if(password.isEmpty()){passwordText.setError("password is empty");}
        if(confirmPassword.isEmpty()){confirmPasswordText.setError("confirmPassword is empty");}
        if (!name.isEmpty()&&!surname.isEmpty()&&!username.isEmpty()&&!password.isEmpty()&&password.equals(confirmPassword)
        &&!birthDate.isEmpty()
        ){appViewModel.addUser(username,password,name,surname,birthDate);
            getActivity().getSupportFragmentManager().popBackStack();}
        }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_register:
               if(checkUser()){
            addUser();
               }else {
                   Toast.makeText(getContext(), "There is such user", Toast.LENGTH_SHORT).show();
               }
            break;
            case R.id.birthDate:

                    pickDate();

                break;
    }
}

    private boolean checkUser() {
        Cursor cursor=appViewModel.readUser();
        while (cursor.moveToNext()){arrayList.add(cursor.getString(cursor.getColumnIndex(Constants.Constant.UserName)));}
        if(arrayList.contains(usernameText.getEditText().getText().toString())){return false;}
        else {return true;}

    }


    private void pickDate() {
        Calendar cal =Calendar.getInstance();

        int year=cal.get(Calendar.YEAR);
        int month=cal.get(Calendar.MONTH);
        int day=cal.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog=new DatePickerDialog(Objects.requireNonNull(getActivity()),this,year,month,day);
DatePicker datePicker=dialog.getDatePicker();
datePicker.setMaxDate(cal.getTimeInMillis());

            dialog.show();
        }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        month=month+1;
        String date=day+"/"+month+"/"+day;

        birthTextView.setText(date);

    }
}
