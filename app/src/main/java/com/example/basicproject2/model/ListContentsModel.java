package com.example.basicproject2.model;

public class ListContentsModel {
    private  int id;
    private int favorite;
    private String category1;
    private String category2;
    private String text;
    private int image;

    public ListContentsModel(int id, int favorite, String category1, String category2, String text, int image) {
        this.id = id;
        this.favorite = favorite;
        this.category1 = category1;
        this.category2 = category2;
        this.text = text;
        this.image = image;
    }

    public void setFavorite(int favorite) {
        this.favorite = favorite;
    }

    public int getId() {
        return id;
    }

    public int getFavorite() {
        return favorite;
    }

    public String getCategory1() {
        return category1;
    }

    public String getCategory2() {
        return category2;
    }

    public String getText() {
        return text;
    }

    public int getImage() {
        return image;
    }
}
