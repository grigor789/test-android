package com.example.basicproject2.model;

public class UserModel {
    private String id;
    private String username;
    private String password;
    private String name;
    private String lastName;
    private String age;



    public UserModel(String id, String username, String password, String name, String lastName, String age) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }
}



