package com.example.basicproject2.projectHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.example.basicproject2.constants.Constants;

public class AppDataBase extends SQLiteOpenHelper {

    public static final String Data_Base_Name = "App Database";
    public static final int Data_Base_Version = 1;
    private Context context;

    public AppDataBase(@Nullable Context context) {
        super(context, Data_Base_Name, null, Data_Base_Version);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query = "CREATE TABLE " + Constants.Constant.TABLE_NAME +
                " (" + Constants.Constant.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Constants.Constant.UserName + " TEXT, " +
                Constants.Constant.Password + " TEXT, " +
                Constants.Constant.Name + " TEXT, " +
                Constants.Constant.LastName + " TEXT, " +
                Constants.Constant.Age + " TEXT);";
        sqLiteDatabase.execSQL(query);
        String query2 = "CREATE TABLE " + Constants.Constant.TABLE_NAME2 +
                " (" + Constants.Constant.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Constants.Constant.Favorite + " NUMBER, " +
                Constants.Constant.PicName + " TEXT, " +
                Constants.Constant.Picture + " NUMBER, " +
                Constants.Constant.Category1 + " TEXT, " +
                Constants.Constant.Category2 + " TEXT);";
        sqLiteDatabase.execSQL(query2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Constants.Constant.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Constants.Constant.TABLE_NAME2);
    }

    public Cursor readUsers(SQLiteDatabase sqLiteDatabase) {
        String query = " SELECT * FROM " + Constants.Constant.TABLE_NAME + " ORDER BY ID ASC ";
        return sqLiteDatabase.rawQuery(query, null);
    }

    public void addUser(SQLiteDatabase sqLiteDatabase, String username, String password, String name, String lastName,
                        String age) {
        ContentValues contentValuesAdd = new ContentValues();
        contentValuesAdd.put(Constants.Constant.UserName, username);
        contentValuesAdd.put(Constants.Constant.Password, password);
        contentValuesAdd.put(Constants.Constant.Name, name);
        contentValuesAdd.put(Constants.Constant.LastName, lastName);
        contentValuesAdd.put(Constants.Constant.Age, age);


        long result = sqLiteDatabase.insert(Constants.Constant.TABLE_NAME, null, contentValuesAdd);
        if (result == -1) {
            Toast.makeText(context, "Not inserted", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Successfully inserted", Toast.LENGTH_SHORT).show();
        }
    }

    public void updateUser(SQLiteDatabase sqLiteDatabase, String username, String password, String name, String lastName,
                           String age, String row_id) {
        ContentValues contentValuesUpdate = new ContentValues();
        contentValuesUpdate.put(Constants.Constant.UserName, username);
        contentValuesUpdate.put(Constants.Constant.Password, password);
        contentValuesUpdate.put(Constants.Constant.Name, name);
        contentValuesUpdate.put(Constants.Constant.LastName, lastName);
        contentValuesUpdate.put(Constants.Constant.Age, age);
        sqLiteDatabase.update(Constants.Constant.TABLE_NAME, contentValuesUpdate, Constants.Constant.ID + "=?", new String[]{row_id});
    }

    public void delete(SQLiteDatabase sqLiteDatabase, String row_id) {

        sqLiteDatabase.delete(Constants.Constant.TABLE_NAME, Constants.Constant.ID + "=?", new String[]{row_id});
    }

    //secondTable

    public Cursor readContent(SQLiteDatabase sqLiteDatabase) {
        String query = " SELECT * FROM " + Constants.Constant.TABLE_NAME2 + " ORDER BY ID ASC ";
        return sqLiteDatabase.rawQuery(query, null);
    }

    public void addContent(SQLiteDatabase sqLiteDatabase, int favorite, String text, int picture, String category1, String category2) {
        ContentValues contentValuesAdd = new ContentValues();
        contentValuesAdd.put(Constants.Constant.Favorite, favorite);
        contentValuesAdd.put(Constants.Constant.Category1, category1);
        contentValuesAdd.put(Constants.Constant.Category2, category2);
        contentValuesAdd.put(Constants.Constant.PicName, text);
        contentValuesAdd.put(Constants.Constant.Picture, picture);

        sqLiteDatabase.insert(Constants.Constant.TABLE_NAME2, null, contentValuesAdd);

    }

    public void updateContent(SQLiteDatabase sqLiteDatabase, int favorite, String category1, String category2, String text, int picture, String row_id) {
        ContentValues contentValuesUpdate = new ContentValues();
        contentValuesUpdate.put(Constants.Constant.Favorite, favorite);
        contentValuesUpdate.put(Constants.Constant.Category1, category1);
        contentValuesUpdate.put(Constants.Constant.Category2, category2);
        contentValuesUpdate.put(Constants.Constant.PicName, text);
        contentValuesUpdate.put(Constants.Constant.Picture, picture);

        sqLiteDatabase.update(Constants.Constant.TABLE_NAME2, contentValuesUpdate, Constants.Constant.ID + "=?", new String[]{row_id});
    }

    public Cursor carContent(SQLiteDatabase sqLiteDatabase) {
        String query = " SELECT * FROM " + Constants.Constant.TABLE_NAME2 + " WHERE " +
                Constants.Constant.Category1 + "  LIKE 'car' " +
                " ORDER BY ID ASC ";
        return sqLiteDatabase.rawQuery(query, null);
    }

    public Cursor phoneContent(SQLiteDatabase sqLiteDatabase) {
        String query = " SELECT * FROM " + Constants.Constant.TABLE_NAME2 + " WHERE " +
                Constants.Constant.Category1 + "  LIKE 'phone' " +
                " ORDER BY ID ASC ";
        return sqLiteDatabase.rawQuery(query, null);
    }

    public Cursor footballContent(SQLiteDatabase sqLiteDatabase) {
        String query = " SELECT * FROM " + Constants.Constant.TABLE_NAME2 + " WHERE " +
                Constants.Constant.Category1 + "  LIKE 'football' " +
                " ORDER BY ID ASC ";
        return sqLiteDatabase.rawQuery(query, null);
    }
}
