package com.example.basicproject2.repository;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.basicproject2.R;
import com.example.basicproject2.fragments.fragments.inside.PhoneFragment;
import com.example.basicproject2.projectHelper.AppDataBase;

public class Repository {

    private AppDataBase appDataBase;

    public Cursor readUser(Context context) {
        appDataBase = new AppDataBase(context);
        return appDataBase.readUsers(appDataBase.getReadableDatabase());
    }

    public void addUser(Context context, String username, String password,
                        String name, String lastName, String age) {
        appDataBase = new AppDataBase(context);
        appDataBase.addUser(appDataBase.getWritableDatabase(), username, password, name, lastName, age);

    }

    public void updateUser(Context context, String username, String password, String name,
                           String lastName, String age,
                            String row_id) {
        appDataBase = new AppDataBase(context);
        appDataBase.updateUser(appDataBase.getWritableDatabase(), username, password, name, lastName, age,
                 row_id);
    }

    public void deleteUser(Context context, String row_id) {
        appDataBase = new AppDataBase(context);
        appDataBase.delete(appDataBase.getWritableDatabase(), row_id);
    }
//second table

    public Cursor readContent(Context context){
        appDataBase = new AppDataBase(context);
        return appDataBase.readContent(appDataBase.getReadableDatabase());
    }
    public void addContent(Context context,  int favorite, String text, int picture, String category1, String category2 ) {
        appDataBase = new AppDataBase(context);
        appDataBase.addContent(appDataBase.getWritableDatabase(),favorite,text,picture,category1,category2);

    }

    public void updateContent(Context context,   int favorite, String text, int picture, String category1, String category2,String row_id) {

        appDataBase.updateContent(appDataBase.getWritableDatabase(), favorite,category1,category2,text,picture,
                row_id);
    }
    public Cursor carContent(Context context){
        appDataBase = new AppDataBase(context);

      return   appDataBase.carContent(appDataBase.getReadableDatabase());
    }
    public Cursor phoneContent(Context context){
        appDataBase=new AppDataBase(context);
        return appDataBase.phoneContent(appDataBase.getReadableDatabase());
    }
    public Cursor footballContent(Context context){
        appDataBase=new AppDataBase(context);
        return appDataBase.footballContent(appDataBase.getReadableDatabase());
    }

}