package com.example.basicproject2.viewModel;


import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.database.Cursor;

import androidx.annotation.NonNull;

import androidx.lifecycle.AndroidViewModel;

import com.example.basicproject2.projectHelper.AppDataBase;
import com.example.basicproject2.repository.Repository;

public class AppViewModel extends AndroidViewModel {
    @SuppressLint("StaticFieldLeak")
    private Context context;

    public AppViewModel(@NonNull Application application) {
        super(application);
       context= application.getApplicationContext();
    }


   private  Repository repository=new Repository();

    public Cursor readUser() {

        return repository.readUser(context);
    }

    public void addUser( String username, String password,
                        String name, String lastName, String age) {

        repository.addUser(context, username, password, name, lastName, age);
    }

    public void updateUser( String username, String password, String name,
                           String lastName, String age, String row_id) {

        repository.updateUser(context, username, password, name, lastName, age,
                 row_id);
    }

    public void deleteUser( String row_id) {

      repository.deleteUser(context, row_id);
    }
    public Cursor readContent() {

        return repository.readContent(context);
    }

    //content
    public void addContent(  int favorite, String text, int picture, String category1, String category2 ) {

        repository.addContent(context,favorite,text,picture,category1,category2 );
    }

    public void updateContent( int favorite, String text, int picture, String category1, String category2, String row_id ) {

        repository.updateContent(context, favorite,text,picture,category1,category2, row_id);
    }
    public Cursor carContent(){
     return   repository.carContent(context);
    }
    public Cursor phoneContent(){
        return   repository.phoneContent(context);
    }
    public Cursor footballContent(){
        return   repository.footballContent(context);
    }


}
